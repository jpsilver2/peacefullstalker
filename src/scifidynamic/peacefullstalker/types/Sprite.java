/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.objects.gameobjects;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joml.Vector2f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import scifidynamic.peacefullstalker.toolbox.Loader;

/**
 *
 * @author Matrix
 */
public class Sprite {
    
    public int SPRITE_ID;
    private int ID;
    private float sprite_speed;
    private int[] TEXTURE_IDS;
    private Vector2f size;
    private static final String TEXTURE_FOLDER = "Textures/Sprites/";
    
    public static void unbindTexture(){
        GL11.glBindTexture(GL_TEXTURE_2D, 0);
    }
    
    public int getWidth(){
        return (int)this.size.x;
    }
    
    public int getHeight(){
        return (int)this.size.y;
    }
    
    private void createSprite(File spriteFile){
        try {
            DataInputStream dataInputStream = new DataInputStream(new FileInputStream(spriteFile));
            dataInputStream.readUTF();
            this.sprite_speed = dataInputStream.readInt();
            TEXTURE_IDS = new int[dataInputStream.readShort()];
            for(int i = 0; i < TEXTURE_IDS.length; i ++){
                byte[] buffer = new byte[dataInputStream.readInt()];
                ByteBuffer imageBuffer = BufferUtils.createByteBuffer(buffer.length);
                dataInputStream.read(buffer);
                imageBuffer.put(buffer);
                imageBuffer.flip();
                TEXTURE_IDS[i] = Loader.loadTexture(imageBuffer, (size = new Vector2f()));
            }
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Sprite.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Sprite.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void bindTexture(int frame){
        GL11.glBindTexture(GL_TEXTURE_2D, TEXTURE_IDS[(int)frame]);
        
    }
    
    public int getID(){
        return ID;
    }
    
    public Sprite(String sprite_name){
        createSprite(new File(TEXTURE_FOLDER + sprite_name));
    }

    public int getCurrentTextureId(int frame) {
        return TEXTURE_IDS[(int)frame];
    }

    public short getMaxFrames() {
        return (short)this.TEXTURE_IDS.length;
    }

    public int getTextureId(int texture_index) {
        return TEXTURE_IDS[texture_index];
    }

    public double getSpriteSpeed() {
        return this.sprite_speed;
    }
    
    public void setSpriteSpeed(float sprite_speed){
        this.sprite_speed = sprite_speed;
    }
}
