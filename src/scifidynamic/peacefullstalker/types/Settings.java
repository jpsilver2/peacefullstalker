/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.types;

import com.sun.javafx.geom.Vec3f;
import java.text.ParseException;
import java.util.Map;

/**
 *
 * @author Matrix
 */
public class Settings {
    
    private int SCREEN_WIDTH;
    private int SCREEN_HEIGHT;
    private Vec3f DEFAULT_COLOR;
    private String TITLE;
    private boolean IS_FULLSCREEN = false;
    private boolean ENABLE_VSYNC = false;
    
    public Settings(Map<String, String> raw_settings){
        this.SCREEN_WIDTH = strToInt(raw_settings.get("SCREEN_WIDTH"));
        this.SCREEN_HEIGHT = strToInt(raw_settings.get("SCREEN_HEIGHT"));
        this.DEFAULT_COLOR = strToVec3f(raw_settings.get("DEFAULT_COLOR"));
        this.TITLE = raw_settings.get("GAME_TITLE");
        this.IS_FULLSCREEN = strToBoolean(raw_settings.get("FULLSCREEN"));
        this.ENABLE_VSYNC = strToBoolean(raw_settings.get("VSYNC"));
    }
    
    public boolean getEnablevSync(){
        return ENABLE_VSYNC;
    }
    
    private static boolean strToBoolean(String str){
        try{
        return Boolean.parseBoolean(str);
        }catch(NumberFormatException e){
            System.err.println("Failed parsing string : " + str + " to boolean");
            e.printStackTrace();
            return false;
        }
    }
    
    private static Vec3f strToVec3f(String str){
        try{
            if(!str.contains(",")){
                throw new NumberFormatException("Not a valid Vec3f String!");
            }
            String[] vec3_str = str.split(",");
            float x = strToFloat(vec3_str[0]);
            float y = strToFloat(vec3_str[1]);
            float z = strToFloat(vec3_str[2]);
            return new Vec3f(x, y, z);
        }catch (NumberFormatException e){
            e.printStackTrace();
            return new Vec3f();
        }
    }
    
    private static float strToFloat(String str){
        try{
        return Float.parseFloat(str);
        }catch(NumberFormatException e){
            System.err.println("Failed parsing string : " + str + " to float");
            e.printStackTrace();
            return -1.0F;
        }
    }
    
    private static int strToInt(String str){
        try{
        return Integer.parseInt(str);
        }catch(NumberFormatException e){
            System.err.println("Failed parsing string : " + str + " to integer");
            e.printStackTrace();
            return -1;
        }
    }

    public Vec3f getDefaultColor() {
        return this.DEFAULT_COLOR;
    }

    public boolean isFullscreen() {
        return this.IS_FULLSCREEN;
    }
    
    public int getScreenWidth(){
        return this.SCREEN_WIDTH;
    }
    public int getScreenHeight(){
        return this.SCREEN_HEIGHT;
    }
    
    public String getTitle(){
        return this.TITLE;
    }
    
    
}
