/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.batches;

import java.util.ArrayList;
import java.util.List;
import scifidynamic.peacefullstalker.objects.gameobjects.Entity;
import scifidynamic.peacefullstalker.toolbox.Loader;

/**
 *
 * @author Matrix
 */
class EntityBatch {
    private static final short MAX_OBJECTS = 2048;
    private List<Integer> openBatches = new ArrayList<>();
    private List<Integer> closedBatches = new ArrayList<>();
    private Entity[] created_entities = new Entity[MAX_OBJECTS];
    private static int current_position = 0;

    public int createEntity(int sprite_id, Loader LOADER) {
        int new_id = -1;
        if(openBatches.size() > 0){
            new_id = openBatches.get(0);
            openBatches.remove(0);
        }else{
            new_id = current_position ++;
        }
        created_entities[new_id] = new Entity(sprite_id, LOADER);
        return new_id;
    }

    public Entity getEntityById(int entity_id) {
        return created_entities[entity_id];
    }
    
}
