/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.batches;

import java.util.ArrayList;
import java.util.List;
import org.joml.Vector2f;
import scifidynamic.peacefullstalker.objects.GameObject;
import scifidynamic.peacefullstalker.objects.SpriteBatch;
import scifidynamic.peacefullstalker.objects.gameobjects.Entity;
import scifidynamic.peacefullstalker.objects.gameobjects.Sprite;
import scifidynamic.peacefullstalker.toolbox.Loader;

/**
 *
 * @author Matrix
 */
public class ObjectBatch {
    private static final short MAX_OBJECTS = 2048;

    public static Entity getEntityById(int entity_id) {
        return ENTITY_BATCH.getEntityById(entity_id);
    }

    private List<Integer> openBatches = new ArrayList<>();
    private List<Integer> closedBatches = new ArrayList<>();
    private GameObject[] created_objects = new GameObject[MAX_OBJECTS];
    private String[] object_names = new String[MAX_OBJECTS];
    
    private final Loader LOADER;
    
    private static final SpriteBatch SPRITE_BATCH = new SpriteBatch();
    private static final EntityBatch ENTITY_BATCH = new EntityBatch();
    
    public static Sprite getSpriteByID(int sprite_id) {
        return SPRITE_BATCH.getSpriteById(sprite_id);
    }
    
    public ObjectBatch(Loader loader){
        LOADER = loader;
    }
    
    private static short current_position = 0;
    
    public int createObject(String name, String sprite_file, String sprite_name, Vector2f position){
        int sprite_id = SPRITE_BATCH.createSprite(sprite_name, sprite_file);
        return createObject(name, position, sprite_id);
    }
    
    public int createObject(String name, Vector2f position, int sprite_id){
        return createObject(name, position, 0.0F, new Vector2f(1, 1), sprite_id);
    }
    
    public int createObject(String name, Vector2f position, float angle, int sprite_id){
        return createObject(name, position, angle, new Vector2f(1, 1), sprite_id);
    }
    
    public int createObject(String name, Vector2f position, float angle, int entity_id, int sprite_id){
        return createObject(name, position, angle, new Vector2f(1, 1), entity_id, sprite_id);
    }
    
    public int createObject(String name, Vector2f position, float angle, Vector2f scale, int sprite_id){
        int entity_id = ENTITY_BATCH.createEntity(sprite_id, LOADER);
        return createObject(name, position, angle, scale, entity_id, sprite_id);
    }
    
    public int createObject(GameObject object, String sprite_name, String sprite_file){
        int new_id = -1;
        if(openBatches.size() > 0){
            new_id = openBatches.get(0);
            openBatches.remove(0);
        }else{
            new_id = current_position ++;
        }
        int sprite_id = SPRITE_BATCH.createSprite(sprite_name, sprite_file);
        int entity_id = ENTITY_BATCH.createEntity(sprite_id, LOADER);
        object_names[new_id] = object.getName();
        object.setId(new_id);
        object.setSpriteId(sprite_id);
        object.setEntityId(entity_id);
        created_objects[new_id] = object;
        closedBatches.add(new_id);
        created_objects[new_id].onCreate();
        return new_id;
    }
    
    public int createObject(GameObject object, int sprite_id){
        int new_id = -1;
        if(openBatches.size() > 0){
            new_id = openBatches.get(0);
            openBatches.remove(0);
        }else{
            new_id = current_position ++;
        }
        int entity_id = ENTITY_BATCH.createEntity(sprite_id, LOADER);
        object_names[new_id] = object.getName();
        object.setId(new_id);
        object.setSpriteId(sprite_id);
        object.setEntityId(entity_id);
        created_objects[new_id] = object;
        closedBatches.add(new_id);
        created_objects[new_id].onCreate();
        return new_id;
    }
    
    public int createObject(String name, Vector2f position, float angle, Vector2f scale, int entity_id, int sprite_id){
        int new_id = -1;
        if(openBatches.size() > 0){
            new_id = openBatches.get(0);
            openBatches.remove(0);
        }else{
            new_id = current_position ++;
        }
        object_names[new_id] = name;
        created_objects[new_id] = new GameObject(new_id,
                                                name, 
                                                position,
                                                angle,
                                                scale,
                                                entity_id,
                                                sprite_id);
        closedBatches.add(new_id);
        created_objects[new_id].onCreate();
        return new_id;
    }
    
    public void remove_object(int object_id){
        openBatches.add(object_id);
        created_objects[object_id].destroy();
    }
    
    public void collectTrash(){
        for(Integer nullable : openBatches){
            created_objects[nullable] = null;
            closedBatches.remove(nullable);
        }
        System.gc();
    }
    
    public void stepBeginObjects(double dt){
        for(Integer object : closedBatches){
            created_objects[object].beginStep(dt);
        }
    }
    
    public void stepObjects(double dt){
        for(Integer object : closedBatches){
            created_objects[object].step(dt);
        }
    }
    
    public void stepEndObjects(double dt){
        for(Integer object : closedBatches){
            created_objects[object].endStep(dt);
        }
    }
    
    public void drawBegin(double dt){
        for(Integer object : closedBatches){
            created_objects[object].beginDraw(dt);
        }
    }
    
    public void draw(double dt){
        for(Integer object : closedBatches){
            created_objects[object].draw(dt);
        }
    }
    
    public void drawEnd(double dt){
        for(Integer object : closedBatches){
            created_objects[object].endDraw(dt);
        }
    }

    public void cleanUp() {
        //TODO: Add cleanup code; oops. lol ill do this later
    }

    public int createObject(String name, Vector2f position, String sprite_file) {
        return createObject(name, sprite_file, name, position);
    }

    public GameObject getObjectById(int object_id) {
        return created_objects[object_id];
    }

    public void createSprites(String sprite_name, String sprite_file) {
        SPRITE_BATCH.createSprite(sprite_name, sprite_file);
    }

    public void createObject(GameObject object) {
        String spr_name = object.getSpriteName();
        int sprite_id = SPRITE_BATCH.getSpriteByName(spr_name);
        createObject(object, sprite_id);
    }
    
}

