/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.shaders.main;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import scifidynamic.peacefullstalker.toolbox.FileDecoder;

/**
 *
 * @author Matrix
 */
public abstract class Shader {
    
    private int shader_id;
    
    public Shader(String filename, int shaderType){
        try {
            shader_id = createShader(filename, shaderType);
        } catch (Exception ex) {
            Logger.getLogger(Shader.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    protected int createShader(String filename, int shaderType) throws Exception {
        int shader = 0;
        try {
            shader = ARBShaderObjects.glCreateShaderObjectARB(shaderType);
             
            if(shader == 0)
                return 0;
             
            ARBShaderObjects.glShaderSourceARB(shader, FileDecoder.readFileAsString(filename));
            ARBShaderObjects.glCompileShaderARB(shader);
             
            if (ARBShaderObjects.glGetObjectParameteriARB(shader, ARBShaderObjects.GL_OBJECT_COMPILE_STATUS_ARB) == GL11.GL_FALSE)
                throw new RuntimeException("Error creating shader: " + getLogInfo(shader));
             
            return shader;
        }
        catch(Exception exc) {
            ARBShaderObjects.glDeleteObjectARB(shader);
            throw exc;
        }
    }
    
    public int getShaderID(){
        return shader_id;
    }
    
    public static String getLogInfo(int obj) {
        return ARBShaderObjects.glGetInfoLogARB(obj, ARBShaderObjects.glGetObjectParameteriARB(obj, ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB));
    }
    
}
