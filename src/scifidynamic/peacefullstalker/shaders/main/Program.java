/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.shaders.main;
import java.nio.FloatBuffer;
import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;

/**
 *
 * @author Matrix
 */
public abstract class Program {
    
    protected int program_id;
    private int vertex_shader;
    private int fragment_shader;
    
    public Program(int vertex_shader, int fragment_shader){
        this.vertex_shader = vertex_shader;
        this.fragment_shader = fragment_shader;
        program_id = GL20.glCreateProgram();
        
        GL20.glAttachShader(program_id, vertex_shader);
        GL20.glAttachShader(program_id, fragment_shader);
         
        GL20.glLinkProgram(program_id);
        if (GL20.glGetShaderi(program_id, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.err.println(Shader.getLogInfo(program_id));
            return;
        }
         
        GL20.glValidateProgram(program_id);
        if (GL20.glGetShaderi(program_id, GL20.GL_COMPILE_STATUS) == GL11.GL_FALSE) {
            System.err.println(Shader.getLogInfo(program_id));
            return;
        }
        loadUniforms();
        loadUniformVariables();
    }
    
    public void start(){
        GL20.glUseProgram(program_id);
    }
    
    public void stop(){
        GL20.glUseProgram(0);
    }
    
    protected void bindAttribute(int attribute, String name){
        GL20.glBindAttribLocation(program_id, attribute, name);
    }
  
    public abstract void loadUniformVariables();
    public abstract void loadUniforms();
    
    public void setIntegerUniform(int index, int value){
        GL20.glUniform1i(index, value);
    }

    public void setFloatUniform(int index, float value) {
        GL20.glUniform1f(index, value);
    }
    
    public void cleanUp(){
        stop();
        GL20.glDetachShader(program_id, vertex_shader);
        GL20.glDetachShader(program_id, fragment_shader);
        GL20.glDeleteProgram(program_id);
    }

    public int getUniformLocation(String name) {
        return GL20.glGetUniformLocation(program_id, name);
    }

    public void setMatrix4(int loc_matrix, Matrix4f matrix) {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        matrix.get(buffer);
        GL20.glUniformMatrix4fv(loc_matrix, false, buffer);
    }
}
