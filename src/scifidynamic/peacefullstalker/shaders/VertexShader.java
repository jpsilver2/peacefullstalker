/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.shaders;

import org.lwjgl.opengl.GL20;
import scifidynamic.peacefullstalker.shaders.main.Shader;

/**
 *
 * @author Matrix
 */
public class VertexShader extends Shader{
    
    private static final String FILENAME = "Shaders/Sprite.vs";
    
    public VertexShader() {
        super(FILENAME, GL20.GL_VERTEX_SHADER);
    }
    
}
