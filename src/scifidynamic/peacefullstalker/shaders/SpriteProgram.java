/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.shaders;

import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import org.lwjgl.opengl.GL13;
import scifidynamic.peacefullstalker.objects.gameobjects.Camera;
import scifidynamic.peacefullstalker.shaders.main.Program;

/**
 *
 * @author Matrix
 */
public class SpriteProgram extends Program{
    
    private int loc_diffuse;
    private int loc_transformation_matrix;
    private int loc_projection_matrix;
    private int loc_view_matrix;
    private Matrix4f transformation_matrix;
    private Matrix4f view_matrix;
    
    private int current_texture_id;
    
    public SpriteProgram() {
        super(new VertexShader().getShaderID(), new FragmentShader().getShaderID());
        Camera.setupProjection();
        loadUniforms();
    }
    
    @Override
    public void loadUniformVariables() {
        super.setIntegerUniform(loc_diffuse, 0);
        super.setMatrix4(loc_transformation_matrix, transformation_matrix);
        super.setMatrix4(loc_projection_matrix, Camera.getProjectionMatrix());
        super.setMatrix4(loc_view_matrix, view_matrix);
    }

    public int getProgramID() {
        return program_id;
    }

    public void start() {
        super.start(); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void update(int currentTextureId, Matrix4f transformation_matrix, Matrix4f view_matrix){
        super.bindAttribute(0, "in_Position");
        super.bindAttribute(0, "in_Coords");
        this.current_texture_id = currentTextureId;
        this.transformation_matrix = transformation_matrix;
        this.view_matrix = view_matrix;
        activateTextures();
        loadUniforms();
        loadUniformVariables();
    }
    
    private void activateTextures(){
        GL13.glActiveTexture(GL13.GL_TEXTURE0);
        GL11.glBindTexture(GL_TEXTURE_2D, current_texture_id);
    }

    @Override
    public void loadUniforms() {
        loc_transformation_matrix = super.getUniformLocation("in_mTransformationMatrix");
        loc_projection_matrix = super.getUniformLocation("in_mProjectionMatrix");
        loc_view_matrix = super.getUniformLocation("in_mViewMatrix");
        loc_diffuse = super.getUniformLocation("diffuse");
    }
    
}
