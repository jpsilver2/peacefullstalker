/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.objects.gameobjects;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import scifidynamic.peacefullstalker.display.Screen.Screen;
import static org.lwjgl.opengl.GL11.*;
import scifidynamic.peacefullstalker.toolbox.Maths;
import scifidynamic.peacefullstalker.types.Settings;
/**
 *
 * @author Matrix
 */
public class Camera {
    
    private static final float FOV = 60;
    private static final float NEAR_PLANE = 0.0001f;
    private static final float FAR_PLANE = 1000;
    private static float aspect_ratio = 0;
    private static Matrix4f mProjectionMatrix;
    
    private Vector3f position = new Vector3f(0, 0, 0);
    private float pitch = 0;
    private float yaw = 0;
    private float roll = 0;
    
    public Camera(){
        
    }
    
    public static void setupProjection(){
        aspect_ratio = (float) Screen.getSettings().getScreenWidth()/ (float) Screen.getSettings().getScreenHeight();
 
        mProjectionMatrix = new Matrix4f();
        mProjectionMatrix.setPerspective(FOV, aspect_ratio, NEAR_PLANE, FAR_PLANE);
    }
    
    public static float getAspectRatio(){
        return Camera.aspect_ratio;
    }
    
    public static Matrix4f getProjectionMatrix(){
        return Camera.mProjectionMatrix;
    }
    
    public void update(double dt){
        
    }
    
    public Matrix4f getViewMatrix(){
        return Maths.createViewMatrix(position, pitch, yaw, roll);
    }

    public void translate(float f, float i, float i0) {
        this.position.x += f;
        this.position.y += i;
        this.position.z += i0;
    }

    public void setPosition(Vector3f position) {
        this.position = new Vector3f(position.x, position.y, position.z);
    }

    public Vector3f getPosition() {
        return this.position;
    }
    
}
