/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.objects.gameobjects;
import scifidynamic.peacefullstalker.batches.ObjectBatch;
import scifidynamic.peacefullstalker.main.Game;
import scifidynamic.peacefullstalker.objects.RawModel;
import scifidynamic.peacefullstalker.toolbox.Loader;

/**
 *
 * @author Matrix
 */
public class Entity {
    private int ID;
    private static RawModel entity_model;
    public static int ENTITY_IDS;
    
    private static float[] createVertices(float width, float height){
        return new float[] {
            0, 1, 0,
            0,0,0,
            1, 0, 0,
            1, 1, 0 };
    }
    
    private static float[] createTexCoords(float width, float height){
        return new float[] { 1,0, 
            1,1, 
            0,1, 
            0,0 };
    }
    
    private static final int[] indices = new int[] { 0, 1, 3,3, 1, 2};
    
    public RawModel getModel() {
        return this.entity_model;
    }
    
    public Entity(int sprite_id, Loader loader){
        Sprite sprite = ObjectBatch.getSpriteByID(sprite_id);
        if(entity_model == null){
        entity_model = loader.loadToVAO(
                Entity.createVertices(sprite.getWidth(), 
                        sprite.getHeight()),
                Entity.indices, 
                Entity.createTexCoords(sprite.getWidth(), sprite.getHeight()));
        }
        this.ID = ENTITY_IDS ++;
    }
    
    public Entity(){
        this.ID = ENTITY_IDS ++;
    }
    
    public int getID(){
        return ID;
    }
    
}
