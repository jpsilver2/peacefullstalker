/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.objects;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import scifidynamic.peacefullstalker.objects.gameobjects.Entity;
import scifidynamic.peacefullstalker.objects.gameobjects.Sprite;
import scifidynamic.peacefullstalker.toolbox.Loader;

/**
 *
 * @author Matrix
 */
public class SpriteBatch {
    private static final short MAX_OBJECTS = 2048;
    private List<Integer> openBatches = new ArrayList<>();
    private List<Integer> closedBatches = new ArrayList<>();
    private Sprite[] created_sprites = new Sprite[MAX_OBJECTS];
    private String[] sprite_name = new String[MAX_OBJECTS];
    private static int current_position = 0;

    public int createSprite(String sprite_name, String file_name) {
        int new_id = -1;
        if(openBatches.size() > 0){
            new_id = openBatches.get(0);
            openBatches.remove(0);
        }else{
            new_id = current_position ++;
        }
        this.created_sprites[new_id] = new Sprite(file_name);
        this.sprite_name[new_id] = sprite_name;
        return new_id;
    }
    
    public Sprite getSpriteById(int sprite_id){
        if(sprite_id == -1)
            return null;
        return created_sprites[sprite_id];
    }

    public int getSpriteByName(String spr_name) {
        int index = 0;
        for(int i = 0; i < MAX_OBJECTS; i ++){
            if(sprite_name[i] == null){
                continue;
            }
            if(sprite_name[i].equals(spr_name)){
                return index;
            }
        }
        return -1;
    }
}
