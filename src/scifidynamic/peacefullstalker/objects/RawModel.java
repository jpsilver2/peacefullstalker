/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.objects;

/**
 *
 * @author Matrix
 */
public class RawModel {
    
    private int vao_id;
    private int vertex_count;
    
    public RawModel(int vao_id, int vertex_count){
        this.vao_id = vao_id;
        this.vertex_count = vertex_count;
    }
    
    public int getVaoId(){
        return this.vao_id;
    }
    
    public int getVertxCount(){
        return this.vertex_count;
    }
    
}
