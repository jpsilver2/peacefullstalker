/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.objects;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import scifidynamic.peacefullstalker.batches.ObjectBatch;

/**
 *
 * @author Matrix
 */
public class Scene {
    protected int scene_index;
    private static int scenes = 0;
    private List<GameObject> objects_to_load = new ArrayList<>();
    private List<String> sprite_names = new ArrayList<>();
    private List<String> sprite_files = new ArrayList<>();
    private List<Integer> loaded_objects = new ArrayList<>();
    private final ObjectBatch OBJECT_MANAGER;
    
    public Scene(List<GameObject> loadObjects, List<String> sprite_names, List<String> sprite_files, ObjectBatch batch){
        this.scene_index = scenes ++;
        this.objects_to_load = loadObjects;
        this.sprite_names = sprite_names;
        this.sprite_files = sprite_files;
        OBJECT_MANAGER = batch;
    }
    
    public void loadObjects(){
        OBJECT_MANAGER.collectTrash();
        for(int i = 0; i < sprite_names.size(); i ++){
            OBJECT_MANAGER.createSprites(sprite_names.get(i), sprite_files.get(i));
        }
        for(int i = 0; i < objects_to_load.size(); i ++){
            GameObject current_obj = objects_to_load.get(i);
            OBJECT_MANAGER.createObject(current_obj);
        }
    }
    
    public void destroy(){
        for(Integer object : loaded_objects){
            OBJECT_MANAGER.remove_object(object);
        }
    }

    public int getId() {
        return this.scene_index;
    }
    
}
