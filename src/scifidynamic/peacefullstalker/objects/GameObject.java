/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.objects;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import scifidynamic.peacefullstalker.batches.ObjectBatch;
import scifidynamic.peacefullstalker.main.Game;
import scifidynamic.peacefullstalker.display.Rendering.Renderer;
import scifidynamic.peacefullstalker.objects.gameobjects.Entity;
import scifidynamic.peacefullstalker.objects.gameobjects.Sprite;
import scifidynamic.peacefullstalker.shaders.SpriteProgram;
import scifidynamic.peacefullstalker.toolbox.Maths;

/**
 *
 * @author Matrix
 */
public class GameObject {
    private int entity_id;
    private int sprite_id;
    private int id;
    private Vector2f position = new Vector2f();
    private float angle = 0;
    private Vector2f scale = new Vector2f(1, 1);
    private SpriteProgram sprite_program;
    private boolean isDestroyed = false;
    private float current_frame;
    private double image_speed;
    private short max_frames;
    private String name;
    private float scale_x = 0;
    private float scale_y = 0;
    private Vector2f offset = new Vector2f(0, 0);
    private String sprite_name;
    
    public GameObject(String name, Vector2f position){
        this.name = name;
        this.position = new Vector2f(position.x, position.y);
        angle = 0;
    }
    
    public GameObject(String name, Vector2f position, String sprite_name){
        this.name = name;
        this.position = new Vector2f(position.x, position.y);
        this.sprite_name = sprite_name;
        angle = 0;
    }
    
    public Vector2f getSpriteSize(){
        Sprite spr = ObjectBatch.getSpriteByID(sprite_id);
        return new Vector2f(spr.getWidth(), spr.getHeight());
    }
    
    private void setScale(Vector2f scale){
        this.scale = new Vector2f(-scale.x, scale.y);
    }
    
    public void setOffset(float x, float y){
        offset = new Vector2f(x, y);
    }
    
    public void onCreate(){
        
    }

    public GameObject(int object_id, String name, Vector2f position, float angle, Vector2f scale, int entity_id, int sprite_id) {
        this.id = object_id;
        this.name = name;
        this.position = new Vector2f(position.x, position.y);
        this.angle = angle;
        this.scale = new Vector2f(scale.x, scale.y);
        this.entity_id = entity_id;
        this.sprite_id = sprite_id;
        initImageDetails();
    }
    
    public void setImageSpeed(float image_speed){
        this.image_speed = image_speed;
    }
    
    private void initImageDetails(){
        Sprite spr = ObjectBatch.getSpriteByID(sprite_id);
        image_speed = spr.getSpriteSpeed();
        max_frames = spr.getMaxFrames();
        float w = spr.getWidth();
        float h = spr.getHeight();
        float max = (float)Math.max(w, h);
        scale_x = w/max + w;
        scale_y = h/max + h;
    }

    public void destroy() {
        this.isDestroyed = true;
    }
    
    private void updateFrame(double dt){
        this.current_frame += (float)(image_speed*dt);
        if(this.current_frame >= max_frames){
            this.current_frame = 0;
        }
        if(this.current_frame < 0){
            this.current_frame = this.max_frames-1;
        }
    }

    public void beginStep(double dt) {
        updateFrame(dt);
    }

    public void step(double dt) {
        
    }

    public void endStep(double dt) {
        
    }

    public void beginDraw(double dt) {
        
    }

    public void draw(double dt) {
        
    }
    
    public void translate(float x, float y){
        this.position.x += x;
        this.position.y += y;
    }
    
    public float getImageSpeed(){
        return (float)this.image_speed;
    }
    
    public void addRotation(float angle){
        this.angle += angle;
    }
    
    private Matrix4f createTransformationMatrix(){
        return Maths.createTransformationMatrix(position,
                                                offset,
                                                this.angle,
                                                new Vector2f(this.scale.x*scale_x, 
                                                             this.scale.y*scale_y));
    }
    
    private void updateSpriteProgram(int current_texture_id){
        sprite_program.update(current_texture_id, 
                createTransformationMatrix(), 
                Renderer.getViewMatrix());
    }
    
    private void startSpriteProgram(Sprite current_sprite){
        if(sprite_program == null){
            sprite_program = new SpriteProgram();
        }
        sprite_program.start();
        updateSpriteProgram(
        current_sprite.getTextureId((int)this.current_frame));
    }
    
    public int getCurrentFrame(){
        return (int)this.current_frame;
    }
    
    public void setCurrentFrame(int frame){
        this.current_frame = frame;
    }

    public void endDraw(double dt) {
        Entity this_entity = ObjectBatch.getEntityById(entity_id);
        Sprite this_sprite = ObjectBatch.getSpriteByID(sprite_id);
        
        RawModel model = this_entity.getModel();
       
        startSpriteProgram(this_sprite);
        
        GL30.glBindVertexArray(model.getVaoId());
        GL20.glEnableVertexAttribArray(0);
        GL20.glEnableVertexAttribArray(1);
        GL11.glDrawElements(GL_TRIANGLES, model.getVertxCount(), GL11.GL_UNSIGNED_INT, 0);
        GL20.glDisableVertexAttribArray(0);
        GL20.glDisableVertexAttribArray(1);
        GL30.glBindVertexArray(0);
        sprite_program.stop();
    }

    public void setScale(float x_scale, float y_scale) {
        this.scale = new Vector2f(x_scale, y_scale);
    }

    public void setId(int new_id) {
        this.id = new_id;
    }

    public void setSpriteId(int sprite_id) {
        this.sprite_id = sprite_id;
        initImageDetails();
    }

    public void setEntityId(int entity_id) {
        this.entity_id = entity_id;
    }

    public String getName() {
        return this.name;
    }

    public String getSpriteName() {
        return this.sprite_name;
    }
    
    public Vector2f getPosition(){
        return this.position;
    }

    public void setPosition(float x, float y) {
        this.position = new Vector2f(x, y);
    }
    
}
