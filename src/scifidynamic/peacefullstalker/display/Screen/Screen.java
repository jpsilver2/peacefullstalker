/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.display.Screen;

import java.nio.IntBuffer;
import org.lwjgl.Version;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MAJOR;
import static org.lwjgl.glfw.GLFW.GLFW_CONTEXT_VERSION_MINOR;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_ESCAPE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_CORE_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_FORWARD_COMPAT;
import static org.lwjgl.glfw.GLFW.GLFW_OPENGL_PROFILE;
import static org.lwjgl.glfw.GLFW.GLFW_RELEASE;
import static org.lwjgl.glfw.GLFW.glfwCreateWindow;
import static org.lwjgl.glfw.GLFW.glfwDefaultWindowHints;
import static org.lwjgl.glfw.GLFW.glfwGetPrimaryMonitor;
import static org.lwjgl.glfw.GLFW.glfwGetVideoMode;
import static org.lwjgl.glfw.GLFW.glfwGetWindowSize;
import static org.lwjgl.glfw.GLFW.glfwInit;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwPollEvents;
import static org.lwjgl.glfw.GLFW.glfwSetKeyCallback;
import static org.lwjgl.glfw.GLFW.glfwSetWindowPos;
import static org.lwjgl.glfw.GLFW.glfwSetWindowShouldClose;
import static org.lwjgl.glfw.GLFW.glfwShowWindow;
import static org.lwjgl.glfw.GLFW.glfwSwapBuffers;
import static org.lwjgl.glfw.GLFW.glfwSwapInterval;
import static org.lwjgl.glfw.GLFW.glfwWindowHint;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWKeyCallback;
import org.lwjgl.glfw.GLFWVidMode;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import org.lwjgl.system.MemoryStack;
import scifidynamic.peacefullstalker.main.Game;
import scifidynamic.peacefullstalker.main.Game;
import static org.lwjgl.system.MemoryStack.stackPush;
import static org.lwjgl.system.MemoryUtil.NULL;
import scifidynamic.peacefullstalker.input.KeyboardHandler;
import scifidynamic.peacefullstalker.toolbox.FileDecoder;
import scifidynamic.peacefullstalker.types.Settings;

/**
 *
 * @author Matrix
 */
public class Screen {
    
    private static long window;
    private static Settings SETTINGS;
    private static final int GL_MAJOR = 3;
    private static final int GL_MINOR = 2;
    private final Game CURRENT_GAME;
    private static GLFWKeyCallback keyCallback;
    
    public Screen(Game game){
        this.CURRENT_GAME = game;
        SETTINGS = new Settings(FileDecoder.readSettingsFile());  
        init();
    }
    
    private void init() {
        System.out.println("Using LWJGL version: " + Version.getVersion() + "!");
        
	initGLFW();
	
	createWindow();
        
	showWindow();
    }
    
    public static Settings getCurrentSettings(){
        return SETTINGS;
    }
    
    public static long getWindow(){
        return Screen.window;
    }
    
    public static Settings getSettings(){
        return Screen.SETTINGS;
    }

    private void initWindow() {
        try ( MemoryStack stack = stackPush() ) {
            IntBuffer pWidth = stack.mallocInt(1); // int*
            IntBuffer pHeight = stack.mallocInt(1); // int*

            glfwGetWindowSize(window, pWidth, pHeight);

            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

            glfwSetWindowPos(
                window,
                (vidmode.width() - pWidth.get(0)) / 2,
                (vidmode.height() - pHeight.get(0)) / 2
            );
	} 
    }

    private void showWindow() {
        glfwMakeContextCurrent(window);
	glfwSwapInterval((SETTINGS.getEnablevSync()) ? 1 : 0);
	glfwShowWindow(window);
    }
    
    private void setKeyCallback(){
        glfwSetKeyCallback(window, keyCallback = new KeyboardHandler());
    }
    
    private void setWindowHints(){
        glfwDefaultWindowHints();
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GL_MAJOR);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GL_MINOR);
    }
    
    private void initGLFW(){
        GLFWErrorCallback.createPrint(System.err).set();
	if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");
    }
    
    private void createWindow(){
        setWindowHints();
        window = glfwCreateWindow(SETTINGS.getScreenWidth(), SETTINGS.getScreenHeight(), SETTINGS.getTitle(), (SETTINGS.isFullscreen()) ? glfwGetPrimaryMonitor() : NULL, NULL);
	if ( window == NULL ){
            throw new RuntimeException("Failed to create the GLFW window");
        } 
        setKeyCallback();
        initWindow();
    }
    
    public void updateDisplay(){
        glfwSwapBuffers(window); // swap the color buffers
            glfwPollEvents();
    }
}
