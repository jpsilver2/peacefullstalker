/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.display.Rendering;

import java.util.List;
import org.joml.Matrix4f;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_COLOR_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_DEPTH_BUFFER_BIT;
import static org.lwjgl.opengl.GL11.GL_TRIANGLES;
import static org.lwjgl.opengl.GL11.glClear;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import scifidynamic.peacefullstalker.batches.ObjectBatch;
import scifidynamic.peacefullstalker.objects.gameobjects.Camera;
import scifidynamic.peacefullstalker.objects.RawModel;
import scifidynamic.peacefullstalker.objects.gameobjects.Entity;
import scifidynamic.peacefullstalker.toolbox.DeltaTime;

/**
 *
 * @author Matrix
 */
public class Renderer {
    
    private static Matrix4f view_matrix;
    
    public Renderer(){
        
    }
    
    public void prepare(Matrix4f view_matrix){
        this.view_matrix = view_matrix;
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
    }
    
    public void render(RawModel rawModel){
        GL30.glBindVertexArray(rawModel.getVaoId());
        GL20.glEnableVertexAttribArray(0);
        GL11.glDrawElements(GL_TRIANGLES, rawModel.getVertxCount(), GL11.GL_UNSIGNED_INT, 0);
        GL20.glDisableVertexAttribArray(0);
        GL30.glBindVertexArray(0);
    }
    
    public void render(ObjectBatch object_batch, double dt){
        object_batch.drawBegin(dt);
        object_batch.draw(dt);
        object_batch.drawEnd(dt);
    }
    
    public static Matrix4f getViewMatrix(){
        return view_matrix;
    }
}
