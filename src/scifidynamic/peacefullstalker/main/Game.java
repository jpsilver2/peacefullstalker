/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.main;

import java.util.ArrayList;
import java.util.List;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import scifidynamic.peacefullstalker.objects.gameobjects.Camera;
import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.glfwDestroyWindow;
import static org.lwjgl.glfw.GLFW.glfwMakeContextCurrent;
import static org.lwjgl.glfw.GLFW.glfwSetErrorCallback;
import static org.lwjgl.glfw.GLFW.glfwTerminate;
import static org.lwjgl.glfw.GLFW.glfwWindowShouldClose;
import org.lwjgl.opengl.GL;
import static org.lwjgl.opengl.GL11.glClearColor;
import scifidynamic.peacefullstalker.batches.ObjectBatch;
import scifidynamic.peacefullstalker.display.Rendering.Renderer;
import scifidynamic.peacefullstalker.objects.RawModel;
import scifidynamic.peacefullstalker.toolbox.Loader;
import scifidynamic.peacefullstalker.display.Screen.Screen;
import static scifidynamic.peacefullstalker.display.Screen.Screen.getSettings;
import static scifidynamic.peacefullstalker.display.Screen.Screen.getWindow;
import scifidynamic.peacefullstalker.objects.GameObject;
import scifidynamic.peacefullstalker.objects.Scene;
import scifidynamic.peacefullstalker.objects.gameobjects.Entity;
import scifidynamic.peacefullstalker.toolbox.DeltaTime;

/**
 *
 * @author Matrix
 */
public abstract class Game {
    
    private List<Scene> scenes = new ArrayList<>();
    private int prev_scene = -1;

    public static Matrix4f getViewMatrix() {
        return camera.getViewMatrix();
    }

    public static int createSprite(String sprite_name, String sprite_file) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private final Screen SCREEN;
    private static final Renderer RENDERER = new Renderer();
    private static final DeltaTime DELTA_TIME = new DeltaTime();
    private static final ObjectBatch OBJECT_MANAGER = new ObjectBatch(new Loader());
    protected static Camera camera;
    
    public int addScene(List<GameObject> scene_objects, List<String> sprite_names, List<String> sprite_files){
        this.scenes.add(new Scene(scene_objects, sprite_names, sprite_files, OBJECT_MANAGER));
        return scenes.size()-1;
    }
    
    public void loadScene(int scene_index){
        if(prev_scene != -1){
            scenes.get(prev_scene).destroy();
        }
        scenes.get(scene_index).loadObjects();
    }
    
    public Game(){
        this.SCREEN = new Screen(this);
        camera = new Camera();
        initGame();
    }
    
    protected void setObjectScale(int object, float x_scale, float y_scale){
        OBJECT_MANAGER.getObjectById(object).setScale(x_scale, y_scale);
    }
    
    protected int createObject(String name, Vector2f position, String sprite_file){
        return OBJECT_MANAGER.createObject(name, position, sprite_file);
    }
    
    protected int createObject(GameObject object, String sprite_name, String sprite_file){
        return OBJECT_MANAGER.createObject(object, sprite_name, sprite_file);
    }
    
    public abstract void checkKeyCodes();
    
    private void initGame(){
        initLoop();
        gameLoop();
        endGame();
    }
    
    public GameObject getObjectById(int object_id){
            return OBJECT_MANAGER.getObjectById(object_id);
    }
    
    protected abstract void load();
    
    protected void update(double dt){
        OBJECT_MANAGER.stepBeginObjects(dt);
        OBJECT_MANAGER.stepObjects(dt);
        OBJECT_MANAGER.stepEndObjects(dt);
    }
    
    protected abstract void draw(double dt);
    
    private void gameLoop() {
	while ( !glfwWindowShouldClose(getWindow()) ) {  
            DELTA_TIME.startLoop();
            update(DELTA_TIME.getDeltaTime());
            draw(DELTA_TIME.getDeltaTime());
            runRenderer(camera.getViewMatrix(), DELTA_TIME.getDeltaTime());
            SCREEN.updateDisplay();
            DELTA_TIME.endLoop();
	}
    }
    
    private void runRenderer(Matrix4f viewMatrix, double dt){
        RENDERER.prepare(viewMatrix);
        RENDERER.render(OBJECT_MANAGER, dt);
    }
    
    private void initClearColor(){
        glClearColor(getSettings().getDefaultColor().x, 
                getSettings().getDefaultColor().y, 
                getSettings().getDefaultColor().z, 1.0f);
    }
    
    private void initContext(){
        glfwMakeContextCurrent(getWindow());
        GL.createCapabilities();
    }
    
    private void initLoop(){
        initContext();
        load();
	initClearColor();
    }
    
    private void endGame(){
        OBJECT_MANAGER.cleanUp();
        glfwFreeCallbacks(getWindow());
	glfwDestroyWindow(getWindow());

	glfwTerminate();
	glfwSetErrorCallback(null).free();
    }
    
    public Camera getCamera(){
        return this.camera;
    }
    
}
