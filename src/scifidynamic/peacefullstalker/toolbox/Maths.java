/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.toolbox;

import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import org.joml.Vector4f;
import scifidynamic.peacefullstalker.objects.gameobjects.Camera;


/**
 *
 * @author Matrix
 */
public class Maths {
    
    public static Matrix4f createTransformationMatrix(Vector2f translation, Vector2f offset, float angle, Vector2f scale){
        return createTransformationMatrix(new Vector3f(translation.x, translation.y, getZ()),
                new Vector3f(offset.x, offset.y, 0),
                angle, scale);
    }
    
    public static float getZ(){
        return -25.0F * Camera.getAspectRatio();
    }
    
    public static Matrix4f createTransformationMatrix(Vector3f translation, Vector3f offset, float angle, Vector2f scale){
        Matrix4f matrix = new Matrix4f();
        matrix.identity();
        matrix.translate(translation);
        matrix.translate(offset);
        matrix.rotate((float)Math.toRadians(angle), 0, 0, 1);
        matrix.scale(scale.x, scale.y, 1);
        matrix.translate(new Vector3f(-offset.x, -offset.y, -offset.z));
        return matrix;
    }
    
    public static Matrix4f createViewMatrix(Vector3f translation, float pitch, float yaw, float roll){
        Matrix4f matrix = new Matrix4f();
        matrix.rotate((float)Math.toRadians(pitch), 1, 0, 0);
        matrix.rotate((float)Math.toRadians(yaw), 0, 1, 0);
        matrix.rotate((float)Math.toRadians(roll), 0, 0, 1);
        matrix.translate(-translation.x, -translation.y, -translation.z);
        return matrix;
    }
    
}
