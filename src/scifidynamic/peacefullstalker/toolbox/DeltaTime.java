/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.toolbox;

import java.util.Date;
import org.lwjgl.glfw.GLFW;

/**
 *
 * @author Matrix
 */
public class DeltaTime {
    
    double lastTime = GLFW.glfwGetTime();
    double delta = 0;
    double fps;
    double time = 0;
    double frames = 0;
    double ms;
    double test = 0;
    
    public void startLoop(){
        lastTime = GLFW.glfwGetTime();
        test += 30* delta;
    }
    
    public void endLoop(){
        double currentTime = GLFW.glfwGetTime();
        double t = (currentTime - lastTime);
        time += t;       
        frames = 1.0/t;
        delta = t;
        ms = frames/60.666;
        if(time >= 1.0){
            System.out.println(frames+ " FPS");
            System.out.println(test + " test");
            test = 0;
            frames = 0;
            time = 0;      
        }
        
        
    }
    
    public double getDeltaTime(){
        return delta;
    }
    
}
