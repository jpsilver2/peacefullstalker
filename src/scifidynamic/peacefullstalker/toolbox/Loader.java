/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic.peacefullstalker.toolbox;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.List;
import org.joml.Vector2f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import static org.lwjgl.opengl.GL11.GL_CLAMP;
import static org.lwjgl.opengl.GL11.GL_NEAREST;
import static org.lwjgl.opengl.GL11.GL_REPEAT;
import static org.lwjgl.opengl.GL11.GL_RGB;
import static org.lwjgl.opengl.GL11.GL_RGBA;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_2D;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MAG_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_MIN_FILTER;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_S;
import static org.lwjgl.opengl.GL11.GL_TEXTURE_WRAP_T;
import static org.lwjgl.opengl.GL11.GL_TRUE;
import static org.lwjgl.opengl.GL11.GL_UNSIGNED_BYTE;
import static org.lwjgl.opengl.GL11.glTexImage2D;
import static org.lwjgl.opengl.GL11.glTexParameterf;
import static org.lwjgl.opengl.GL11.glTexParameteri;
import org.lwjgl.opengl.GL12;
import static org.lwjgl.opengl.GL12.GL_CLAMP_TO_EDGE;
import org.lwjgl.opengl.GL13;
import static org.lwjgl.opengl.GL14.GL_GENERATE_MIPMAP;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import org.lwjgl.opengl.GL30;
import org.lwjgl.stb.STBImage;
import static org.lwjgl.stb.STBImage.stbi_failure_reason;
import static org.lwjgl.stb.STBImage.stbi_set_flip_vertically_on_load;
import org.lwjgl.system.MemoryStack;
import static org.lwjgl.system.MemoryStack.stackPush;
import scifidynamic.peacefullstalker.objects.RawModel;

/**
 *
 * @author Matrix
 */
public class Loader {
    
    private List<Integer> vao_ids = new ArrayList<>();
    private List<Integer> vbo_ids = new ArrayList<>();
    
    public RawModel loadToVAO(float[] positions, int[] indices, float[] texCoords){
        int vao_id = crateVAO();
        bindIndicesBuffer(indices);
        storeInAttributeList(0, positions, 3);
        storeInAttributeList(1, texCoords, 2);
        unbindVAO();
        return new RawModel(vao_id, indices.length);
    }
    
    private int crateVAO(){
        glEnableVertexAttribArray(1);
        int vao_id = GL30.glGenVertexArrays();
        GL30.glBindVertexArray(vao_id);
        
        vao_ids.add(vao_id);
        
        return vao_id;
    }
    
    private void storeInAttributeList(int attr_number, float[] data, int vertex_size){
        int vbo_id = GL15.glGenBuffers();
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo_id);
        FloatBuffer buffer = dataToFloatBuffer(data);
        GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
        GL20.glVertexAttribPointer(attr_number, vertex_size, GL11.GL_FLOAT, false, 0, 0);
        GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
        
        vbo_ids.add(vbo_id);
    }
    
    private void bindIndicesBuffer(int[] indices){
        int vbo_id = GL15.glGenBuffers();
        vbo_ids.add(vbo_id);
        GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, vbo_id);
        IntBuffer buffer = dataToIntBuffer(indices);
        GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, buffer, GL15.GL_STATIC_DRAW);
    }
    
    private IntBuffer dataToIntBuffer(int[] data){
        IntBuffer int_buffer = BufferUtils.createIntBuffer(data.length);
        int_buffer.put(data);
        int_buffer.flip();
        return int_buffer;
    }
    
    private void unbindVAO(){
        GL30.glBindVertexArray(0);
    }
    
    private FloatBuffer dataToFloatBuffer(float[] data){
        FloatBuffer floatBuffer = BufferUtils.createFloatBuffer(data.length);
        floatBuffer.put(data);
        floatBuffer.flip();
        return floatBuffer;
    }
    
    public void cleanUp(){
        for(int vao : vao_ids){
            GL30.glDeleteVertexArrays(vao);
        }
        
        for(int vbo : vbo_ids){
            GL15.glDeleteBuffers(vbo);
        }
    }
    
    public static int loadTexture(ByteBuffer imageBuffer, Vector2f size){
        ByteBuffer image;
        int texture_id = GL11.glGenTextures();
        GL11.glBindTexture(GL_TEXTURE_2D, texture_id);
        MemoryStack stack = stackPush();
        IntBuffer w = stack.mallocInt(1);
        IntBuffer h = stack.mallocInt(1);
        IntBuffer comp = stack.mallocInt(1);
        stbi_set_flip_vertically_on_load(true);
        image = STBImage.stbi_load_from_memory(imageBuffer, w, h, comp, 4);
        if (image == null) {
            throw new RuntimeException("Failed to load a texture file!"
            + System.lineSeparator() + stbi_failure_reason());
        }
        
        int width = w.get();
        int height = h.get();
        
        size.x = width;
        size.y = height;
        
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
        
        return texture_id;
    }
}
