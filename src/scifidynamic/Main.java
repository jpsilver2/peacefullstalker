/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package scifidynamic;

import org.joml.Vector2f;
import scifidynamic.peacefullstalker.main.Game;
import scifidynamic.peacefullstalker.objects.GameObject;

/**
 * This is a example class to show off minor features of the framework/Game.
 * @author Matrix
 */
public class Main {
    private static int obj_running_girl;
    private static int obj_blocks[] = new int[64];
    private static float scale = 1;
    private static float direction = -0.001f;
    public static void main(String[] args){
        Game game = new Game() {
            @Override
            public void checkKeyCodes() {
                //Check key presses(For movement and input)
            }

            @Override
            protected void load() {
                /*
                I created my own file type called a .spr(sprite file) i wrote the program that converts a series of png's to a single file.
                it also stores the amount of frames and the frame speed, as well as the original size of the sprite.
                I am also going to make the framework so that you can use one sprite multiple times, that is why there is an object and sprite id.
                */
                for(int x = -32; x < 32; x ++){
                    obj_blocks[x+32] = createObject("obj_background", new Vector2f(x*32, 128), "block.spr"); 
                }
                obj_running_girl = createObject("obj_running_girl", new Vector2f(0.0f, 0.0f), "running_girl.spr");
            }
            /*
            This code creates an animation of sorts, with the girl walking on a pane while 
            growing and shrinking and the camera moving right and down slightly fowllowing the girls movement
            */
            @Override
            protected void update(double dt){
                super.update(dt);
                float move_up = -0.02f;
                GameObject girl = getObjectById(obj_running_girl);
                girl.translate(0, move_up);
                setObjectScale(obj_running_girl, scale, scale);
                scale += direction;
                for(int i = 0; i < 64; i ++){
                    GameObject current_block = getObjectById(obj_blocks[i]);
                    current_block.translate(0.3f*scale, direction*128 + move_up);
                    if(current_block.getPosition().x > (32*32)){
                        current_block.setPosition(-32*32, current_block.getPosition().y);
                    }
                }
                if(scale < 0.5 || scale > 1.5){
                    direction *= -1;
                }
            }

            @Override
            protected void draw(double dt) {
                //What should it draw(extra eg gui and stuff like that)
            }
        };
    }
    
}
