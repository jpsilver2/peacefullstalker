#version 140

uniform sampler2D current_texture_id;

in vec2 tex_Coords;

void main() {
	vec2 tex_coords = tex_Coords;
	vec4 color = texture2D(current_texture_id, tex_coords);
	if(color.a < 0.5){
		color = vec4(0, 0, 0, 1);
	}
    gl_FragColor = color;
}