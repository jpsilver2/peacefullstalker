#version 140

in vec3 in_Position;
in vec2 in_Coords;

uniform mat4 in_mTransformationMatrix;
uniform mat4 in_mProjectionMatrix;
uniform mat4 in_mViewMatrix;

out vec2 tex_Coords;

void main(){
    gl_Position = in_mProjectionMatrix * in_mViewMatrix * in_mTransformationMatrix * vec4(in_Position, 1); //Transform the vertex position
	tex_Coords = in_Coords;
}